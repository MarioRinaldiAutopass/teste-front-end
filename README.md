# Teste front-end #

Olá pessoa, 

Este teste tem por finalidade entender seu conhecimento com a linguagem Javascript, a biblioteca ReactJS e noções de CSS/HTML. Analisando qualidade de código e organização.

## Passo a passo: ##
  1. Efetuar fork deste projeto;
  2. O prazo para desenvolvimento é informado por email de instrução do teste, contados a partir do recebimento do mesmo;
  3. Após a conclusão, nos avise por e-mail, junto com o link do seu projeto.

## Instruções gerais ##
  1. Implementar um layout responsivo, mobile-first e em grid;
  2. Seguir o mockup (pode utilizar qualquer coisa que ajude no desenvolvimento: bootstrap, material UI, qualquer outro de sua preferencia);
  3. Utilizar fonte Poppins;

## API: ##
  1. Dados do usuário:
      * POST: http://5de52b6f712f9b00145143b2.mockapi.io/api/v1/user

  2. Dados do estabelecimento: (informações da empresa e local, horário, preços, adicionais)
      * POST: http://5de52b6f712f9b00145143b2.mockapi.io/api/v1/place 
      * PUT: http://5de52b6f712f9b00145143b2.mockapi.io/api/v1/place/1 


## Links úteis: ##
* MOCKUP: https://xd.adobe.com/view/22e4204b-ef88-4b41-6e1e-b72fef432927-54d5/
* FONTE: https://fonts.google.com/specimen/Poppins

